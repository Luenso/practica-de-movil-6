package solorzanomedinaluis.facci.practica_6_4b;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button btingresar,  btfoto, btsensores, btsalir;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btingresar = findViewById(R.id.btIngresar);
        btfoto = findViewById(R.id.btFoto);
        btsensores = findViewById(R.id.btSensores);
        btsalir = findViewById(R.id.btSalir);
        btsensores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ActivitySensores.class);
                startActivity(intent);
            }
        });
    }
}
