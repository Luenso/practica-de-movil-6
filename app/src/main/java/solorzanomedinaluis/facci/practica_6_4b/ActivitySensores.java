package solorzanomedinaluis.facci.practica_6_4b;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class ActivitySensores extends AppCompatActivity {
    Button btacelerometro, btproximidad, btpresion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensores);
        btacelerometro = findViewById(R.id.btAcelerometrp);
        btproximidad = findViewById(R.id.btProximidad);
        btpresion = findViewById(R.id.btLuz);

        btacelerometro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivitySensores.this, ActivitySensorAcelerometro.class);
                startActivity(intent);
            }
        });
        btproximidad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivitySensores.this, ActivitySensorProximidad.class);
                startActivity(intent);
            }
        });

        btpresion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ActivitySensores.this, ActivitySensorLuz.class);
                startActivity(intent);
            }
        });
    }
}
